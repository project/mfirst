The mfirst Drupal 7 theme is designed and developed with Mobile First in mind.
CSS and JS are held to a minimum.
Basic styles are larger with more contrast for the smaller screens of modern smart phones. 
No Flash is advised because of the focus on "Mobile First" and the simple fact that the current Apple iOS does not natively support flash.
Navigation and block presentation are to the left column.
Content, Views, and Data Visualization are presented in the center.
A right side bar is also available for advertising, forms and blocks.
Multiple additional regions are available.

Should you need any assistance or have feedback to contribute.
Contact the Author at the project page on Drupal.org.
http://drupal.org/user/184811

